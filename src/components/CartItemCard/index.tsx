'use client';

import { useAppDispatch } from "@/hooks/reduxHooks";
import { cartItem } from "@/redux/features/cart/cartSlice";
import { AddCircle, MinusCirlce, Trash } from "iconsax-react";
import { cartActions } from "@/redux/features/cart/cartSlice";

interface props {
  item: cartItem,
  itemIndex: number
}

export default function CartItemCard({item, itemIndex}: props)
{
  const dispatch = useAppDispatch();

  return (
    <div className='bg-white p-5 border-2 rounded-lg'>
      <div className='flex gap-5'>
        <div className='flex items-center'>
        <input type="checkbox" checked={item.isSelected} onChange={() => dispatch(!item.isSelected ? cartActions.selectCartItem(itemIndex) : cartActions.unselectCartItem(itemIndex))} />                    
        </div>
        <div className={`h-[128px] w-[128px] rounded-lg bg-cover bg-center`} style={{backgroundImage: `url('${item.imgUrl}')`}} />
        <div className='flex-1 flex justify-between flex-col'>
          <div>
            <div className='text-lg font-semibold'>
              {item.name}
            </div>
            <div className='flex gap-6'>
              <span>
                {item.price}
              </span>
              <span>
                {item.weight}
              </span>
            </div>
          </div>
          <div className='justify-end flex items-center gap-8'>
            <button className='text-[#8F0A13]' onClick={() => dispatch(cartActions.removeCartItem(itemIndex))}>
              <Trash />
            </button>
            <div className='flex gap-4 flex-wrap'>
              <div className='flex gap-2 items-center rounded-lg bg-white flex-1'>
                <button className='text-[#5D5FEF]' onClick={() => dispatch(cartActions.setCartItemQuantity({index: itemIndex, quantity: item.quantity - 1}))}>
                  <MinusCirlce />
                </button>
                <input type='text' className='font-bold w-[64px] text-center border-2 rounded-lg' value={item.quantity} onChange={e => dispatch(cartActions.setCartItemQuantity({index: itemIndex, quantity: e.target.value}))} />                          
                <button className='text-[#5D5FEF]' onClick={() => dispatch(cartActions.setCartItemQuantity({index: itemIndex, quantity: item.quantity + 1}))}>
                  <AddCircle />
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>              
    </div>
  )
}