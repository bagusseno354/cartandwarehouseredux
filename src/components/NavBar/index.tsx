import clsx from 'clsx';
import { Category2, Shop, ShoppingCart, LogoutCurve, ArrowLeft, Setting, Profile } from 'iconsax-react';

interface props {
    className?: string,
    setIsMenuOpen: Function
}

export default function Menu({className, setIsMenuOpen}: props)
{
    return (
        <div className={clsx('h-full flex justify-between flex-col border-r-2 fixed xl:relative bg-white z-10 overflow-y-scroll', className)}>
        <div>
          <div className='p-6 text-xl font-bold text-[#5D5FEF]'>
            Marketplace
            <button className='absolute right-4 top-4 xl:hidden' onClick={() => setIsMenuOpen(false)}>
              <ArrowLeft />
            </button>
          </div>
          <div className='text-[#98949E]'>
            <div className='px-6 py-2 text-sm'>Menu</div>
            <div className='flex gap-3 flex-col'>
              <button className='px-6 py-2 gap-3 flex items-center justify-between'>
                <div className='flex gap-3 items-center'>
                  <Category2 size={14} />
                  <span>
                    Home
                  </span>
                </div>                
                <span className='rounded-full px-2 text-sm text-white' style={{backgroundImage: 'linear-gradient(#EEA849, #F46B45)'}}>
                  4
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center text-[#5D5FEF]'>
                <ShoppingCart enableBackground='#5D5FEF' size={14} />
                <span>
                  Cart
                </span>
              </button>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <Shop size={14} />
                <span>
                  Shop
                </span>
              </button> 
            </div>
          </div>
          <div className='mt-6 text-[#98949E]'>
            <div className='px-6 py-2 text-sm'>Account</div>
            <div className='flex gap-3 flex-col'>
              <button className='px-6 py-2 flex gap-3 items-center'>
                <Profile size={14} />
                <span>
                  Profile
                </span>
              </button>              
              <button className='px-6 py-2 flex gap-3 items-center'>
                <Setting size={14} />
                <span>
                  Settings
                </span>
              </button>
            </div>
          </div>
        </div>
        <div className='p-6'>          
          <button className='bg-[#FEF5F6] text-[#8F0A13] flex items-center justify-center gap-2 w-full p-2'>
            <LogoutCurve size={16} />
            <span>
              Log out
            </span>
          </button>
        </div>
      </div>
    )
}