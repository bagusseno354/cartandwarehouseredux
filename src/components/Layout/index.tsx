'use client'

import NavBar from "../NavBar";
import { useState } from 'react';
import { HambergerMenu } from 'iconsax-react';

interface props {
  children?: any
}

export default function Layout({children}: props)
{
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  return (
    <div className='flex h-screen'>
      <NavBar setIsMenuOpen={setIsMenuOpen} className={isMenuOpen ? '' : 'hidden xl:flex'} />
      <div className='flex-1 p-6 flex flex-col overflow-y-scroll'>
        <div className='xl:hidden mb-6'>
          <button onClick={() => setIsMenuOpen(true)}>
            <HambergerMenu size={32} />
          </button>
        </div>
        {children}
      </div>
    </div>
  )
}