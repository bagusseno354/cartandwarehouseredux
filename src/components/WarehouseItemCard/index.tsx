import { useAppDispatch } from "@/hooks/reduxHooks";
import { cartActions, warehouseItem } from "@/redux/features/cart/cartSlice";
import { AddCircle, MinusCirlce, Trash } from "iconsax-react";

interface props {
  item: warehouseItem,
  itemIndex: number
}

export default function WarehouseItemCard({item, itemIndex}: props)
{
  const dispatch = useAppDispatch();

  return (
    <div className='bg-white p-5 border-2 rounded-lg'>
      <div className='flex gap-5'>
        <div className='flex items-center'>
          <input type="checkbox" checked={item.isSelected} onChange={() => dispatch(!item.isSelected ? cartActions.selectWarehouseItem(itemIndex) : cartActions.unselectWarehouseItem(itemIndex))} />                    
        </div>
        <div className='flex-1 flex justify-between flex-col'>
          <div>
            <div className='text-lg font-semibold'>
              {item.name}
            </div>
            <div className='flex gap-6'>
              <span>
                {item.price}
              </span>
              <span>
                {item.weight}
              </span>
              <span>
                {item.quantity} buah
              </span>
            </div>
          </div>
          <div className='justify-end flex items-center gap-8'>
            <button className='text-[#8F0A13]' onClick={() => dispatch(cartActions.removeWarehouseItem(itemIndex))}>
              <Trash />
            </button>            
          </div>
        </div>
      </div>              
    </div>
  )
}