import { createSlice } from '@reduxjs/toolkit';

export type cartItem = {
  name: string,
  imgUrl: string,
  price: number,
  quantity: number,
  weight: number,
  isSelected: boolean,
}

export type warehouseItem = {
  name: string,
  price: number,
  quantity: number,
  weight: number,
  isSelected: boolean,
}

type State = {
  cartItems: cartItem[],
  warehouseItems: warehouseItem[],
};

const initialCartItems: cartItem[] = [
  {
    name: 'Teh Sosro',
    imgUrl: 'https://images.tokopedia.net/img/cache/700/VqbcmM/2020/9/9/1228404f-f2f7-447a-86d4-6559ff6c154f.jpg',
    weight: 1,
    quantity: 5,
    isSelected: false,
    price: 15000
  },
  {
    name: 'LeMinerale',
    imgUrl: 'https://drivethru.klikindomaret.com/t13s/wp-content/uploads/sites/40/2021/05/le-minerale-600ml.jpg',
    weight: 0.6,
    quantity: 2,
    isSelected: false,
    price: 6000
  }
];

const initialWarehouseItems: warehouseItem[] = [
  {
    name: 'Teh Hitam',
    weight: 3,
    quantity: 5,
    isSelected: false,
    price: 15000
  },
  {
    name: 'Teh Merah',
    weight: 5,
    quantity: 2,
    isSelected: false,
    price: 6000
  }
]

export const cartSlice = createSlice({
  name: 'user',
  initialState: {
    cartItems: initialCartItems,
    warehouseItems: initialWarehouseItems
} as State,
  reducers: {
    addCartItem: (state, action) => 
    {
      state.cartItems.push(action.payload);
    },
    removeCartItem: (state, action) => 
    {
      state.cartItems.splice(action.payload, 1);
    },
    removeAllCartItems: (state, action) =>
    {
        state.cartItems = [];
    },
    setCartItemQuantity: (state, action) => 
    {
      state.cartItems[action.payload.index].quantity = action.payload.quantity;
    },
    selectCartItem: (state, action) => 
    {
      state.cartItems[action.payload].isSelected = true;
    },
    unselectCartItem: (state, action) => 
    {
      state.cartItems[action.payload].isSelected = false;
    },
    selectAllCartItems: (state, action) => 
    {
      state.cartItems = state.cartItems.map(cartItem => ({...cartItem, isSelected: true}));
    },
    unselectAllCartItems: (state, action) => 
    {
      state.cartItems = state.cartItems.map(cartItem => ({...cartItem, isSelected: false}));
    },
    addWarehouseItem: (state, action) => 
    {
      state.warehouseItems.push(action.payload);
    },
    removeWarehouseItem: (state, action) => 
    {
      state.warehouseItems.splice(action.payload, 1);
    },
    removeAllWarehouseItems: (state, action) =>
    {
        state.warehouseItems = [];
    },
    selectWarehouseItem: (state, action) => 
    {
      state.warehouseItems[action.payload].isSelected = true;
    },
    unselectWarehouseItem: (state, action) => 
    {
      state.warehouseItems[action.payload].isSelected = false;
    },
    selectAllWarehouseItems: (state, action) => 
    {
      state.warehouseItems = state.warehouseItems.map(warehouseItem => ({...warehouseItem, isSelected: true}));
    },
    unselectAllWarehouseItems: (state, action) => 
    {
      state.warehouseItems = state.warehouseItems.map(warehouseItem => ({...warehouseItem, isSelected: false}));
    },
  },
})

// Action creators are generated for each case reducer function
export const cartActions = cartSlice.actions

export default cartSlice.reducer
