'use client'

import { Edit, Trash, ShieldSearch, ArrowRight, Add, Filter, Refresh, Printer, SearchNormal, AddCircle, MinusCirlce } from 'iconsax-react';
import Layout from '@/components/Layout';
import CartItemCard from '@/components/CartItemCard';
import { useState } from 'react';
import WarehouseItemCard from '@/components/WarehouseItemCard';
import clsx from 'clsx';
import { cartActions, warehouseItem } from '@/redux/features/cart/cartSlice';
import { useAppDispatch, useAppSelector } from '@/hooks/reduxHooks';

export default function Index() 
{
  const [selectedTabIndex, setSelectedTabIndex] = useState(0);

  const cartItems = useAppSelector((state) => state.cart.cartItems);
  const warehouseItems = useAppSelector((state) => state.cart.warehouseItems);
  const dispatch = useAppDispatch();

  const [newWarehouseItem, setNewWarehouseItem] = useState<warehouseItem>({name: '', quantity: 0, weight: 0, price: 0, isSelected: false});
  const [isAddingNewWarehouseItem, setIsAddingNewWarehouseItem] = useState(false);

  return (
    <Layout>
      <div className='flex border-b-2 flex-col xl:flex-row'>
          <div className='pb-4'>
            <div className='text-xl font-bold'>
              Keranjang
            </div>
            <div className='text-[#98949E]'>
              Anda dapat mengatur isi keranjang dan warehouse di sini
            </div>
          </div>
          <div className='flex items-end justify-end flex-1 text-[#98949E]'>
            <button onClick={() => setSelectedTabIndex(0)} className={clsx('px-10 py-3 text-center font-bold', selectedTabIndex == 0 ? 'text-[#5D5FEF] border-b-2 border-b-[#5D5FEF]' : '')}>Keranjang ({cartItems.length})</button>
            <button onClick={() => setSelectedTabIndex(1)} className={clsx('px-10 py-3 text-center font-bold', selectedTabIndex == 1 ? 'text-[#5D5FEF] border-b-2 border-b-[#5D5FEF]' : '')}>Warehouse ({warehouseItems.length})</button>
          </div>
        </div>
        <div className='mt-4 flex gap-4 flex-1 flex-col xl:flex-row relative'>
          {
            selectedTabIndex == 0 &&
            <div className='flex-1'>
              <div className='flex items-center justify-between gap-4'>
                <div className='flex gap-3'>
                  <input type="checkbox" checked={cartItems.every(item => item.isSelected == true)} onChange={() => dispatch(!cartItems.every(item => item.isSelected == true) ? cartActions.selectAllCartItems({}) : cartActions.unselectAllCartItems({}))} /> 
                  <span>
                    Pilih semua
                  </span>
                </div>
                <button onClick={() => dispatch(cartActions.removeAllCartItems({}))} className='bg-[#FEF5F6] text-[#8F0A13] gap-2 p-2 flex items-center gap-3'>
                  <Trash />
                  <span>
                    Hapus Semua
                  </span>
                </button>
              </div>
              <div className='flex gap-4 flex-col mt-4'>            
                {cartItems.map((item, itemIndex) => <CartItemCard key={itemIndex} item={item} itemIndex={itemIndex} />)}
                {
                  cartItems.length <= 0 && 
                  <div className='text-[#98949E]'>
                    Belum ada data.
                  </div>
                }
              </div>
            </div>
          }
          {
            selectedTabIndex == 1 &&
            <div className='flex-1'>
              <div className='flex items-center justify-between gap-4'>
                <div className='flex gap-3'>
                  <input type="checkbox" checked={warehouseItems.every(item => item.isSelected == true)} onChange={() => dispatch(!warehouseItems.every(item => item.isSelected == true) ? cartActions.selectAllWarehouseItems({}) : cartActions.unselectAllWarehouseItems({}))} /> 
                  <span>
                    Pilih semua
                  </span>
                </div>
                <div className='flex gap-4'>
                  <button onClick={() => dispatch(cartActions.removeAllWarehouseItems({}))} className='bg-[#FEF5F6] text-[#8F0A13] gap-2 p-2 flex items-center gap-3 rounded-lg'>
                    <Trash />
                    <span>
                      Hapus Semua
                    </span>
                  </button>
                  <button onClick={() => setIsAddingNewWarehouseItem(true)} className='bg-[#5D5FEF] text-white gap-2 p-2 flex items-center gap-3 rounded-lg'>
                    <AddCircle />
                    <span>
                      Tambah barang
                    </span>
                  </button>
                </div>
              </div>
              <div className='flex gap-4 flex-col mt-4'>            
                {warehouseItems.map((item, itemIndex) => <WarehouseItemCard key={itemIndex} item={item} itemIndex={itemIndex} />)}
                {
                  warehouseItems.length <= 0 && 
                  <div className='text-[#98949E]'>
                    Belum ada data.
                  </div>
                }
              </div>
            </div>
          }
          <div className='xl:max-w-[256px] w-full flex flex-col'>
            <div className='bg-[#5D5FEF] rounded-lg p-4 text-white flex justify-between flex-col sticky top-0' style={{backgroundSize: 'auto 50%', backgroundPosition: 'right 0% bottom 0%', backgroundRepeat: 'no-repeat', backgroundImage: 'url("http://localhost:3000/circles.png")'}}>
              <div className='text-lg font-bold'>Checkout</div>
              <div className='flex justify-between gap-2 mt-4'>
                <div>
                  Produk di keranjang yang dipilih
                </div>
                <div>
                  {cartItems.filter(item => item.isSelected == true).length}/{cartItems.length}
                </div>
              </div>
              <div className='flex justify-between gap-2 mt-2'>
                <div>
                  Produk di warehouse yang dipilih
                </div>
                <div>
                  {warehouseItems.filter(item => item.isSelected == true).length}/{warehouseItems.length}
                </div>
              </div>
              <div className='flex justify-between gap-2 mt-6'>
                <div>
                  Total harga
                </div>
                <div>
                  Rp. {cartItems.filter(item => item.isSelected).reduce((acc, prev) => acc + (prev.price * prev.quantity), 0) + warehouseItems.filter(item => item.isSelected).reduce((acc, prev) => acc + (prev.price * prev.quantity), 0)}
                </div>
              </div>
              <div className='flex justify-between gap-2 mt-2'>
                <div>
                  Total berat
                </div>
                <div>
                  {cartItems.filter(item => item.isSelected).reduce((acc, prev) => acc + (prev.weight * prev.quantity), 0) + warehouseItems.filter(item => item.isSelected).reduce((acc, prev) => acc + (prev.weight * prev.quantity), 0)}kg
                </div>
              </div>
              <button className='px-4 py-2 flex gap-3 items-center justify-center bg-[#FFFFFF33] rounded-lg mt-10'>
                Checkout
                <ArrowRight size={16} />
              </button>
            </div>
          </div>
        </div>
        {
          isAddingNewWarehouseItem &&
          <div className='absolute left-0 right-0 top-0 bottom-0 bg-[#000000bb] z-10 flex items-center justify-center'>
            <div className='bg-white rounded-lg shadow p-4'>
              <div className='text-lg font-bold'>Tambah barang ke warehouse</div>
              <div className='flex gap-4 flex-col'>
                <div className='mt-4'>
                  <div>Nama barang</div>
                  <input type="text" className='border-2 p-2 rounded-lg w-full' value={newWarehouseItem.name} onChange={e => setNewWarehouseItem(prev => ({...prev, name: e.target.value}))} />
                </div>
                <div>
                  <div>Harga/pcs</div>
                  <input type="number" className='border-2 p-2 rounded-lg w-full' value={newWarehouseItem.price} onChange={e => setNewWarehouseItem(prev => ({...prev, price: parseInt(e.target.value)}))} />
                </div>
                <div className="flex gap-4">
                  <div>
                    <div>Berat/pcs</div>
                    <input type="number" className='border-2 p-2 rounded-lg w-full' value={newWarehouseItem.weight} onChange={e => setNewWarehouseItem(prev => ({...prev, weight: parseInt(e.target.value)}))} />
                  </div>
                  <div>
                    <div>Jumlah</div>
                    <input type="number" className='border-2 p-2 rounded-lg w-full' value={newWarehouseItem.quantity} onChange={e => setNewWarehouseItem(prev => ({...prev, quantity: parseInt(e.target.value)}))} />
                  </div>
                </div>
                <div className="flex gap-4">
                  <div>
                    <div>Total berat</div>
                    <input readOnly type="text" className='border-2 p-2 rounded-lg w-full' value={newWarehouseItem.weight * newWarehouseItem.quantity} />
                  </div>
                  <div>
                    <div>Total harga</div>
                    <input type="text" className='border-2 p-2 rounded-lg w-full' value={newWarehouseItem.price * newWarehouseItem.quantity} />
                  </div>
                </div>
              </div>
              <div className='mt-10 flex justify-end'>
                <button onClick={() =>
                {
                  dispatch(cartActions.addWarehouseItem(newWarehouseItem));

                  setNewWarehouseItem({name: '', quantity: 0, weight: 0, price: 0, isSelected: false});
                  setIsAddingNewWarehouseItem(false);

                }} className='bg-[#5D5FEF] text-white gap-2 p-2 flex items-center gap-3 rounded-lg'>
                  <AddCircle />
                  <span>
                    Tambah barang
                  </span>
                </button>
              </div>
            </div>
          </div>
        }
    </Layout>
  );
}