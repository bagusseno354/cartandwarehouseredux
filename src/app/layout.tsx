'use client';

import type { Metadata } from 'next'
import { Quicksand } from 'next/font/google'
import './globals.css'
import { Provider } from 'react-redux'
import store from '@/redux/store';

const quicksand = Quicksand({
  weight: '500',
  subsets: ['latin'],
})

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <Provider store={store}>
      <html lang="en">
        <head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin='use-credentials' />
          <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@300..700&display=swap" rel="stylesheet" />  
        </head>
        <body className={quicksand.className}>{children}</body>
      </html>
    </Provider>
  )
}
